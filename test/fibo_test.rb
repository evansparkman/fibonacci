require 'minitest/autorun'
require './fibo'

describe "Fibonnaci" do
  describe "#fibo" do
    it { fibo(0).must_equal [0] }
    it { fibo(1).must_equal [0, 1, 1] }
    it { fibo(2).must_equal [0, 1, 1, 2] }
    it { fibo(5).must_equal [0, 1, 1, 2, 3, 5, 8] }
    it { fibo(7).must_equal [0, 1, 1, 2, 3, 5, 8, 13, 21] }
  end
end
