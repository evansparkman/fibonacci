def fibo(sequence_number)
  result = sequence_number == 0 ? [0] : [0,1]
  sequence_number.times do
    result << result[-2] + result[-1]
  end
  result
end
